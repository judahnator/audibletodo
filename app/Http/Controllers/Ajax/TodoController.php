<?php

namespace App\Http\Controllers\Ajax;

use App\Todo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TodoController extends Controller
{
    public function index() {
        return response()->json(\Auth::user()->todos);
    }

    public function store(Request $request) {
        dd($request->all());
        $this->validate($request, [
            'notes' => 'text|nullable',
            'file' => 'file|mimes:mp3,wav'
        ]);
        $file = $request->file('file')->getBasename();
        dd($file);
        \Auth::user()->todos()->create([
            'notes' => $request->get('notes'),
            'file' => ''
        ]);
    }

    public function destroy(Todo $todo) {
        // todo
    }
}
