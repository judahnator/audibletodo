@extends('layouts.app')

@section('header')
    <script src="//www.WebRTC-Experiment.com/RecordRTC.js"></script>
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">

            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif

            <div class="panel panel-default">
                <div class="panel-heading">My ToDo Items</div>

                <div class="panel-body">

                    <table class="table table-striped" id="NewTodoItem">
                        <thead>
                        <tr>
                            <th>Record</th>
                            <th>Add Note</th>
                            <th>Submit</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                <input type="file" accept="audio/*">
                            </td>
                            <td>
                                <textarea class="form-control" id="notes" placeholder="Add some notes..."></textarea>
                            </td>
                            <td>
                                <button type="button" class="btn btn-success" id="submit">Submit</button>
                            </td>
                        </tr>
                        </tbody>
                    </table>

                    <table class="table table-striped" id="TodoTable">
                        <thead>
                        <tr>
                            <th>Listen</th>
                            <th>Notes</th>
                            <th>Dismiss</th>
                        </tr>
                        </thead>
                        <tbody></tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
    <script>
        $(document).ready(function() {
            var TodoItems = [];
            $.get('{{ route('ajax.todo.index') }}')
                .done(function (r) {
                    r.forEach(function(item) {
                        $('#TodoTable').find('> tbody').append(
                            '<tr>' +
                                '<td>' +
                                    '<button type="button" onclick="playFile(' + item.id + ')" class="btn btn-info">Listen</button>' +
                                '</td>' +
                                '<td>' +
                                    '<p>' + item.notes + '</p>' +
                                '</td>' +
                                '<td>' +
                                    '<button class="btn btn-danger" type=button" onclick="dismissTodo(' + item.id + ')">Dismiss</button>' +
                                '</td>' +
                            '</tr>'
                        );
                    });
                })
        });

        function playFile(todoId) {
            var a;
        }

        function dismissTodo(todoId) {
            //
        }
    </script>
@endpush
