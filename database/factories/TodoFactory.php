<?php

use Faker\Generator as Faker;

$factory->define(App\Todo::class, function (Faker $faker) {
    return [
        'user_id' => App\User::inRandomOrder()->first()->id,
        'notes' => $faker->sentences(3, true),
        'file' => 'file.wav'
    ];
});
